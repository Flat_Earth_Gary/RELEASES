# MY PUBLIC (and for the most part stable) RELEASES
This is my public repo created for releases. All releases in this repo will be stable as long as nothing else is noted.

<p align='center'>
  <img src='https://gitlab.com/Flat_Earth_Gary/RELEASES/-/raw/master/MailBomber_V4.1/images/pic.png'>
</p>

All of these tools are completely free of charge.
Be aware that I do not take any responsibility with what you do with these tools.

Anyway, hope you enjoy the tools!
Have a great day!


UPDATES!
- V1 First release
- V1.1 NOT STABLE SHOULD NOT BE USED
- V2 Huge update, fixed various bugs
- V2.1 Fixed security flaw
- V3 Added support for larger amounts of mails
- V4 Added GUI
- V4.1 Overall improvments
